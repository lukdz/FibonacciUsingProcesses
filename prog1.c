#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main() {
    HANDLE sem = OpenSemaphore(SYNCHRONIZE | SEMAPHORE_MODIFY_STATE, FALSE, TEXT("Semafor1"));
    int F1=0, F2=0, F3=1;
    bool odp=true;
    while(odp){
        WaitForSingleObject(sem, INFINITE);

        FILE *fp; /* u¿ywamy metody wysokopoziomowej - musimy mieæ zatem identyfikator pliku, uwaga na gwiazdkê! */
        char proc, info;
        if ((fp=fopen("bufor.txt", "r"))==NULL) {
            printf ("Nie mogê otworzyæ pliku do odczytu!\n");
            exit(1);
        }
        fscanf (fp, "%c", &proc);
        if(proc=='M'){
            fscanf (fp, "%c", &info);
            printf("%c", info);
            fclose (fp); /* zamknij plik */
            if(info=='S'){
                F1=0;
                F2=0;
                F3=1;
                FILE *fp; /* u¿ywamy metody wysokopoziomowej - musimy mieæ zatem identyfikator pliku, uwaga na gwiazdkê! */
                char tekst = 'P';
                if ((fp=fopen("bufor.txt", "w"))==NULL) {
                    printf ("Nie mogê otworzyæ pliku do zapisu!\n");
                    exit(1);
                }
                fprintf (fp, "%c", tekst);
                fprintf (fp, "%d", F3);
                fclose (fp); /* zamknij plik */
            }
            if(info=='N'){
                F1=F2;
                F2=F3;
                F3=F1+F2;
                if(F3==5){
                    F3=4; //TEST wykrywanie błędnego wyniku
                }

                FILE *fp; /* u¿ywamy metody wysokopoziomowej - musimy mieæ zatem identyfikator pliku, uwaga na gwiazdkê! */
                char tekst = 'P';
                if ((fp=fopen("bufor.txt", "w"))==NULL) {
                    printf ("Nie mogê otworzyæ pliku do zapisu!\n");
                    exit(1);
                }
                fprintf (fp, "%c", tekst);
                fprintf (fp, "%d", F3);
                fclose (fp); /* zamknij plik */
            }
            if(info=='R'){
                F3=F1+F2;
                FILE *fp; /* u¿ywamy metody wysokopoziomowej - musimy mieæ zatem identyfikator pliku, uwaga na gwiazdkê! */
                char tekst = 'P';
                if ((fp=fopen("bufor.txt", "w"))==NULL) {
                    printf ("Nie mogê otworzyæ pliku do zapisu!\n");
                    exit(1);
                }
                fprintf (fp, "%c", tekst);
                fprintf (fp, "%d", F3);
                fclose (fp); /* zamknij plik */
            }
            if(info=='E'){
                printf("\n", info);
                odp=false;
            }
        }
        else{
            fclose (fp); /* zamknij plik */
        }
        ReleaseSemaphore(sem, 1, NULL);
    }
    return 0;
}
